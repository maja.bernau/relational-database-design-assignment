## Relational Database Design Assignment
Authors:
- [**Patrik Ekman**](https://gitlab.com/patek624)
- [**Jonas Jonsson**](https://gitlab.com/brorjonas)
- [**Maja Bernau**](https://gitlab.com/maja.bernau)

### Task 1: Conceptual ER Diagram
![Task 1: Conceptual ER Diagram](task1-conceptual-er-diagram.drawio.svg)

#### Comments
**Stay** aims to enhance the overall guest experience and operational efficiency: 
- It centralizes a guest's activities and requests during their time at the hotel, check-in and check-outs times, type of stay (leisure/business) etc.
- Aims to enhance communication and coordination between departments to enable a seamless guest experience. Employees from different departments can have real-time access to guest-related information during a guest's stay.
- Historical data on guest stays provides insights for personalizing services and loyalty programs.

**Booking** connects guests with their accommodations before arrival, matching preferences and requirements with available rooms.

### Task 2: Logical ER Diagram

![Task 2: Logical ER Diagram](task2-logical-er-diagram.drawio.svg)

### Task 3: Physical ER Diagram with Design Choices

![Task 3: Physical ER Diagram with Design Choices](task3-physical-er-diagram-with-design-choices.drawio.svg)

#### Entity color coding

 - ![#D5E8D4](https://placehold.co/60x25/D5E8D4/0F9000?text=Green) The central entities including most relevant information connecting other tables with forgin keys.
 - ![#dae8fc](https://placehold.co/60x25/dae8fc/003290?text=Blue) Entities storing information about something concrete.
 - ![#dae8fc](https://placehold.co/60x25/fff2cc/889000?text=Yellow) Junktion table used when many-many relationship occoured. <br/>
 - ![#dae8fc](https://placehold.co/60x25/e1d5e7/90008E?text=Purple) Normalized type entities.


 #### Indexing
 In order to speed up the process for looking up a customer, an index could be created on
 **PersonalNumber** in the **Customers** table. For quick queries on customers.
 Since this would be an equality search, Hash index would be the optimal choice! 


 Another suggestion would be to create an index for **ArrivalDate** in the **Bookings** table.
 This could be beneficial to have a smooth experience for both staff and customers as check-ins might happen every day.
 Search for **ArrivalDate** could also be of the range type, here a B-tree index would also be a solid choice.